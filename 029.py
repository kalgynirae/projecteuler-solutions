#!/usr/bin/python3

s = set(a**b for a in range(2, 101) for b in range(2, 101))
print(len(s))
