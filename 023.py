#!/usr/bin/python3
from kalgynirae.random.maths import factors

def abundant(n):
    return sum(factors(n) - {n}) > n

abundants = set(n for n in range(1, 28123) if abundant(n))
possible = set(range(1, 28123))
yes = set(a + b for a in abundants for b in abundants)
no = possible - yes
print(sum(no))
