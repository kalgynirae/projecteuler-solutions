#include <cstdlib>
#include <iostream>
using namespace std;

int paths(int x, int y, int size) {
    if (x > size || y > size) return 0;
    if (x == size && y == size) return 1;
    return paths(x+1, y, size) + paths(x, y+1, size);
}

int main(int argc, char *argv[]) {
    if (argc == 2) {
        int size = atoi(argv[1]);
        cout << paths(0, 0, size) << endl;
        return 0;
    }
    else {
        cout << "Usage: " << argv[0] << " SIZE" << endl;
        return 1;
    }
}
