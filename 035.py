#/usr/bin/python3
from kalgynirae.random.maths import primes

def circulate(n):
    s = str(n)
    for i in range(len(s)):
        yield int(s[i:] + s[:i])

p = list(primes(1000000))
count = 0
for n in p:
    if all(q in p for q in circulate(n)):
        count += 1
        print(n)
print("count: {}".format(count))
