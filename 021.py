#!/usr/bin/python3

from kalgynirae.random.maths import factors

cache = {}
def d(n):
    if n not in cache:
        result = sum(factors(n) - {n})
        cache[n] = result
        return result
    else:
        return cache[n]

def generate():
    for n in range(1, 10000):
        q = d(n)
        if d(q) == n and q != n:
            print(n)
            yield n

print(sum(generate()))
