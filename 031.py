#!/usr/bin/python3

units = [200, 100, 50, 20, 10, 5, 2, 1]

def ways(amount, units):
    if amount == 0:
        return 1
    elif not units:
        return 0
    n = amount // units[0]
    return sum(ways(amount - i*units[0], units[1:]) for i in range(n+1))
print(ways(200, units))
