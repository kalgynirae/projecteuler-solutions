#!/usr/bin/python3
from itertools import islice, permutations

print([''.join(p) for p in permutations('0123456789')][999999])
