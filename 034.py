#!/usr/bin/python3
from math import factorial

for n in range(1000000):
    if n == sum(factorial(int(d)) for d in str(n)):
        print(n)
