#!/usr/bin/python3

def sumpowdig(n):
    return sum(int(d)**5 for d in str(n))

sum_ = 0
for n in range(2,1000000):
    if sumpowdig(n) == n:
        print(n)
        sum_ += n
print("sum: %d" % sum_)
