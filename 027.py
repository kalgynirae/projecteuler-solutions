#!/usr/bin/python3
from itertools import count
from kalgynirae.random.maths import primes

primes = list(primes(100000))
r = range(-999, 1000)
counts = {}
for a in r:
    for b in r:
        for n in count():
            if n**2 + a*n + b not in primes:
                counts[(a, b)] = n
                break
n, a, b = max((n, a, b) for (a, b), n in counts.items())
print(a * b)
