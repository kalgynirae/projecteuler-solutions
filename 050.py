#!/usr/bin/python3
from kalgynirae.random.maths import primes

p = list(primes(999999))
maxdiff = 0
for a in range(len(p)):
    for b in range(a + maxdiff, len(p)):
        s = sum(p[a:b])
        if s > 999999:
            break
        if s in p:
            if b - a >= maxdiff:
                maxdiff, maxa, maxb = b - a, a, b
print(sum(p[maxa:maxb]))
