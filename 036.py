#!/usr/bin/python3
from itertools import islice

def palindromic(s):
    return all(a == b for a, b in islice(zip(s, s[::-1]), 0, len(s)//2))

def _(n):
    print(n)
    return n

print("sum: %d" % sum(_(n) for n in range(1000000)
                      if palindromic(str(n)) and palindromic(bin(n)[2:])))
