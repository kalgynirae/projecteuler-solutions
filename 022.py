#!/usr/bin/python3
import sys

letters = ' abcdefghijklmnopqrstuvwxyz'
scores = []
with open(sys.argv[1]) as f:
    for pos, name in enumerate(sorted(ln.strip().lower() for ln in f), 1):
        score = sum(letters.find(c) for c in name) * pos
        scores.append(score)
        print('{} {} {}'.format(pos, name, score))
print(sum(scores))
