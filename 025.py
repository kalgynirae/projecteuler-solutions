#!/usr/bin/python3

def fib():
    yield from (1, 1)
    a, b = (1, 1)
    while True:
        a, b = b, a + b
        yield b

for i, s in enumerate((str(n) for n in fib()), 1):
    if len(s) >= 1000:
        print(i)
        break
