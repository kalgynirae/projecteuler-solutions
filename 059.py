#!/usr/bin/python3
from itertools import cycle, product
from string import ascii_lowercase

encrypted = list(map(int, open('059_cipher1.txt').read().strip().split(',')))
decryptions = []
for key in product(ascii_lowercase, repeat=3):
    decrypted = ''.join(chr(b ^ ord(k)) for b, k in zip(encrypted, cycle(key)))
    decryptions.append((decrypted.count(' '), ''.join(key), decrypted))
answer = max(decryptions)
print("# of spaces: {0[0]}\nkey: {0[1]}\nsum of asciis: {1}\ndecrypted: {0[2]}"
      "".format(answer, sum(ord(c) for c in answer[2])))
