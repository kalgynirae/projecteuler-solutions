#!/usr/bin/python3
from itertools import count, cycle
import sys
import time

directions = cycle([
    lambda x, y: (x, y+1),
    lambda x, y: (x+1, y),
    lambda x, y: (x, y-1),
    lambda x, y: (x-1, y)])
size = int(sys.argv[1])
square = [[0] * size for _ in range(size)]
x = size // 2
y = size // 2
square[x][y] = 1
d = next(directions)
newd = next(directions)
for i in count(2):
    # Move in the next direction
    x, y = d(x, y)
    # Set the value
    try:
        square[x][y] = i
    except IndexError:
        break
    # Change direction if possible
    newx, newy = newd(x, y)
    if square[newx][newy] == 0:
        d, newd = newd, next(directions)
#print('\n'.join(' '.join('{:^8}'.format(n) for n in row) for row in square))
diagonal = set()
for x in range(size):
    diagonal.add(square[x][x])
    diagonal.add(square[-x-1][x])
print(sum(diagonal))
