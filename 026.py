#!/usr/bin/python3
from operator import itemgetter

def cycle_length(x):
    """Return the number of digits in the recurring cycle of 1/divisor"""
    # Long division it up!
    remainder = 1
    q = []
    s = set()
    while True:
        quotient = remainder // x
        remainder %= x
        if quotient == 0:
            remainder *= 10
        t = (quotient, remainder)
        if t in s:
            return len(q) - q.index(t)
        else:
            q.append(t)
            s.add(t)

print(max(((i, cycle_length(i)) for i in range(1, 1000)), key=itemgetter(1)))
